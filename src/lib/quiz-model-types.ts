import { Document } from 'mongoose';
import { QuizModelItem } from '../models/quiz/QuizModelItem';

export type QuizModelItemQuestion =
    Document
    & Pick<QuizModelItem, 'sentQuestionIndex' | 'questionList' | 'currentQuestionIndex' | 'name'>
    & { questionCount: number };

export type QuizModelAttendeeQuiz = Document & QuizModelItem & { questionCount: number };
