import { Authorized, Body, BodyParam, Delete, Get, JsonController, Param, Post, Put } from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';
import MemberDAO from '../../db/MemberDAO';
import MotdDAO from '../../db/MotdDAO';
import QuizDAO from '../../db/QuizDAO';
import SettingsDAO from '../../db/SettingsDAO';
import UserDAO from '../../db/UserDAO';
import { UserRole } from '../../enums/UserRole';
import { IMotd } from '../../interfaces/motd/IMotd';
import { IAdminQuiz } from '../../interfaces/quizzes/IAdminQuiz';
import { ISettings } from '../../interfaces/settings/ISettings';
import { IUserSerialized } from '../../interfaces/users/IUserSerialized';
import { MessageOfTheDayItem } from '../../models/motd/MessageOfTheDayItem';
import { SettingsItem } from '../../models/settings/SettingsItem';
import { UserModel, UserModelItem } from '../../models/UserModelItem/UserModel';
import { AbstractRouter } from './AbstractRouter';

@JsonController('/api/v1/admin')
export class AdminRouter extends AbstractRouter {
  @Get('/users') //
  @OpenAPI({
    description: 'Returns all available users',
    security: [{ bearerAuth: [] }],
  })
  @Authorized(UserRole.SuperAdmin) //
  private getUsers(): Promise<Array<IUserSerialized>> {
    return UserModel.find().lean().exec() as Promise<Array<IUserSerialized>>;
  }

  @Delete('/user/:username') //
  @OpenAPI({
    description: 'Removes a given user',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.QuizAdmin, UserRole.SuperAdmin])
  private async deleteUser(@Param('username') username: string): Promise<void> {
    await UserDAO.removeUser((await UserDAO.getUser(username)).id);
  }


  @Put('/user')
  @OpenAPI({
    description: 'Adds a new user or updates an existing one',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.QuizAdmin, UserRole.SuperAdmin])
  private async putUser(
    @BodyParam('originalUser', { required: false }) originalUser: string, //
    @BodyParam('name') name: string, //
    @BodyParam('privateKey') privateKey: string, //
    @BodyParam('passwordHash') passwordHash: string, //
    @BodyParam('tokenHash', { required: false }) tokenHash: string, //
    @BodyParam('userAuthorizations') userAuthorizations: Array<string>, //
    @BodyParam('gitlabToken', { required: false }) gitlabToken: string, //
  ): Promise<UserModelItem> {
    const userData: IUserSerialized = {
      name,
      passwordHash,
      tokenHash,
      privateKey,
      userAuthorizations,
      gitlabToken,
    };

    if (!originalUser) {
      originalUser = name;
    }

    await UserModel.updateOne({ name: originalUser }, userData, {
      upsert: true,
      setDefaultsOnInsert: true,
    }).exec();

    return UserDAO.getUser(originalUser);
  }

  @Get('/quizzes') //
  @OpenAPI({
    description: 'Returns all available quizzes',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.QuizAdmin, UserRole.SuperAdmin])
  private async getQuizzes(): Promise<Array<IAdminQuiz>> {
    return Promise.all(
      (await QuizDAO.getAllQuizzes()).map(async (quiz) => {
        let questionAmount = 0;
        let answerAmount = 0;
        if (Array.isArray(quiz.questionList) && quiz.questionList.length) {
          questionAmount = quiz.questionList.length;
          answerAmount = quiz.questionList
            .map((question) => question.answerOptionList.length)
            .reduce(
              (previousValue, currentValue) => previousValue + currentValue,
            );
        }
        const memberAmount = (await MemberDAO.getMembersOfQuiz(quiz.name))
          .length;

        return {
          state: quiz.state,
          id: quiz._id.toHexString(),
          name: quiz.name,
          expiry: quiz.expiry,
          visibility: quiz.visibility,
          questionAmount,
          answerAmount,
          memberAmount,
        };
      }),
    );
  }

  @Post('/quiz') //
  @OpenAPI({
    description: 'Deactivates a given quiz',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.QuizAdmin, UserRole.SuperAdmin])
  private async updateQuizState(
    @BodyParam('quizname') quizname: string,
  ): Promise<void> {
    const quiz = await QuizDAO.getQuizByName(quizname);
    if (quiz) {
      return QuizDAO.setQuizAsInactive(quizname, quiz.privateKey);
    }
  }

  @Get('/quiz/:id') //
  @OpenAPI({
    description: 'Returns an available quiz by the id',
    security: [{ bearerAuth: [] }],
  })
  @Authorized([UserRole.QuizAdmin, UserRole.SuperAdmin])
  private async getQuiz(@Param('id') quizId: string): Promise<object> {
    return (await QuizDAO.getQuizById(quizId)).toJSON();
  }

  @Delete('/quiz/:quizName') //
  @OpenAPI({
    description: 'Removes a given quiz',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.QuizAdmin, UserRole.SuperAdmin])
  private async deleteQuiz(@Param('quizName') quizName: string): Promise<void> {
    return QuizDAO.removeQuizByName(quizName);
  }

  @Post('/motd') //
  @OpenAPI({
    description: 'Add new message of the day',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.MotdAdmin, UserRole.SuperAdmin])
  private async addNewMessageOfTheDay(
    @Body() newMotd: IMotd,
  ): Promise<MessageOfTheDayItem> {
    return (await MotdDAO.createNewMessageOfTheDay(newMotd)).toJSON();
  }

  @Get('/motd')
  @OpenAPI({
    description: 'Return all availble messages of the day',
    security: [{ bearerAuth: [] }],
  }) //
  private async getAllMessagesOfTheDay(): Promise<Array<IMotd>> {
    return MotdDAO.getAllMessagesOfTheDay();
  }

  @Put('/motd') //
  @OpenAPI({
    description: 'Change informationen of one specific messages of the day',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.MotdAdmin, UserRole.SuperAdmin])
  private async updateSpecificMessageOfTheDay(
    @Body() motdToUpdate: IMotd,
  ): Promise<MessageOfTheDayItem> {
    return MotdDAO.updateSpecificMessageOfTheDay(motdToUpdate);
  }

  @Delete('/motd/:motdId') //
  @OpenAPI({
    description: '',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.MotdAdmin, UserRole.SuperAdmin])
  private async deleteSpecificMessageOfTheDay(
    @Param('motdId') motdId: string,
  ): Promise<boolean> {
    return MotdDAO.deleteSpecificMessageOfTheDay(motdId);
  }

  @Post('/settings')
  @OpenAPI({
    description: 'Add new settings',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.ConfAdmin, UserRole.SuperAdmin])
  private async createNewSettings(
    @Body() newSettings: ISettings,
  ): Promise<SettingsItem> {
    return SettingsDAO.createNewSettings(newSettings);
  }

  @Get('/settings/env')
  @OpenAPI({
    description: '',
    security: [{ bearerAuth: [] }],
  }) //
  private async getSettingsForEnviroment(): Promise<ISettings> {
    return (await SettingsDAO.getConfigurationByConfigName('front_env')).toJSON();
  }

  @Get('/settings/:configName')
  @OpenAPI({
    description: '',
    security: [{ bearerAuth: [] }],
  })
  @Authorized([UserRole.ConfAdmin, UserRole.SuperAdmin])
  private async getSettingsByName(
    @Param('configName') configName: string,
  ): Promise<ISettings> {
    return (await SettingsDAO.getConfigurationByConfigName(configName)).toJSON();
  }

  @Put('/settings')
  @OpenAPI({
    description: 'Change informationen of one specific configuration',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.ConfAdmin, UserRole.SuperAdmin])
  private async updateSpecificSettings(
    @Body() settingsToUpdate: ISettings,
  ): Promise<SettingsItem> {
    return SettingsDAO.updateSettingsInformation(settingsToUpdate);
  }

  @Delete('/settings/:settingId')
  @OpenAPI({
    description: 'Delete one specific configuration',
    security: [{ bearerAuth: [] }],
  }) //
  @Authorized([UserRole.ConfAdmin, UserRole.SuperAdmin])
  private async deleteSpecificSettings(
    @Param('settingId') settingId: string,
  ): Promise<boolean> {
    return SettingsDAO.deleteSettings(settingId);
  }
}
