import { IMemberGroupBase } from '../users/IMemberGroupBase';

export interface IEnvironment {
  _id?: string;
  configName: string;
  config: {
    enableBonusToken: boolean;
    vapidPublicKey: string;
    title: string;
    appName: string;
    sentryDSN?: string;
    backendVersion: string;
    production: boolean;
    stompConfig: {
      endpoint: string, user: string, password: string, vhost: string,
    };
    leaderboardAlgorithm: string;
    leaderboardAmount: number;
    readingConfirmationEnabled: boolean;
    confidenceSliderEnabled: boolean;
    infoAboutTabEnabled: boolean;
    infoProjectTabEnabled: boolean;
    infoBackendApiEnabled: boolean;
    requireLoginToCreateQuiz: boolean;
    showJoinableQuizzes: boolean;
    showPublicQuizzes: boolean;
    forceQuizTheme: boolean;
    loginMechanism: Array<string>;
    showLoginButton: boolean;
    persistQuizzes: boolean;
    availableQuizThemes: Array<string>;
    defaultTheme: string;
    darkModeCheckEnabled: boolean;
    enableTwitter: boolean;
    enableQuizPool: boolean;
    showInfoButtonsInFooter: boolean;
    markdownFilePostfix: string;
    loginButtonLabelConfiguration: string;
    cacheQuizAssets: boolean;
    createQuizPasswordRequired: boolean;
    limitActiveQuizzes: number;
    twitterEnabled: boolean;
    defaultQuizSettings: IDefaultQuizSettings;
  };
}

interface IDefaultAnswerSetting {
  answerText: string;
  isCorrect: boolean;
  configCaseSensitive: boolean;
  configTrimWhitespaces: boolean;
  configUseKeywords: boolean;
  configUsePunctuation: boolean;
}

interface IDefaultQuestionSetting {
  dispayAnswerText: boolean;
  showOneAnswerPerRow: boolean;
  questionText: string;
  timer: number;
  multipleSelectionEnabled: boolean;
  rangeMin: number;
  rangeMax: number;
  correctValue: number;
  answerOptionList: Array<IDefaultAnswerSetting>;
  tags: Array<string>;
  requiredForToken: boolean;
  difficulty: number;
}

interface IDefaultMusicSessionSetting {
  enabled: {
    lobby: boolean;
    countdownRunning: boolean;
    countdownEnd: boolean;
  };
  shared: {
    lobby: boolean;
    countdownRunning: boolean;
    countdownEnd: boolean;
  };
  volumeConfig: {
    global: number;
    lobby: number;
    countdownRunning: number;
    countdownEnd: number;
    useGlobalVolume: boolean;
  };
  titleConfig: {
    lobby: string;
    countdownRunning: string;
    countdownEnd: string;
  };
}

interface IDefaultSessionSetting {
  music: IDefaultMusicSessionSetting;
  nicks: {
    memberGroups: Array<IMemberGroupBase>;
    maxMembersPerGroup: number;
    autoJoinToGroup: boolean;
    blockIllegalNicks: boolean;
    selectedNicks: Array<string>;
  };
  theme: string;
  readingConfirmationEnabled: boolean;
  showResponseProgress: boolean;
  confidenceSliderEnabled: boolean;
  cacheQuizAssets: boolean;
}

interface IDefaultQuizSettings {
  answers: IDefaultAnswerSetting;
  question: IDefaultQuestionSetting;
  sessionConfig: IDefaultSessionSetting;
}
