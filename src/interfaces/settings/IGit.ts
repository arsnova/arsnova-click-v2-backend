export interface IGit {
  _id?: string;
  configName: string;
  config: {
    frontendGitlabId: number;
    backendGitlabId: number;
    gitlabLoginToken: string;
    gitlabHost: string;
    gitlabTargetBranch: string;
  };
}
