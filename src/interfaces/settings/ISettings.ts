export interface ISettings {
  _id?: string;
  configName: string;
  config: any;
}
