export interface ITwitter {
  _id?: string;
  configName: string;
  config: {
    twitterEnabled: boolean;
    twitterConsumerKey: string;
    twitterConsumerSecret: string;
    twitterBearerToken: string;
    twitterAccessTokenKey: string;
    twitterAccessTokenSecret: string;
    twitterSearchKey: string;
  };
}
