export interface IWebNotifications {
  _id?: string;
  configName: string;
  config: {
    vapidPublicKey: string;
    vapidPrivateKey: string;
  };
}
