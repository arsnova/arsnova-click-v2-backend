export interface IMiscellaneous {
  _id?: string;
  configName: string;
  config: {
    chromiumPath: string;
    projectEmail: string;
    logoFilename: string;
  };
}
