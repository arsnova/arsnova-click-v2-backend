export interface IMotd {
  _id?: string;
  mid?: number;
  title: string;
  content: string;
  createdAt: Date;
  updatedAt: Date;
  expireAt: Date;
  isPinned: Boolean;
}
