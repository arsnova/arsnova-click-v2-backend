import { hostname } from 'os';
import path from 'path';
import process from 'process';
import { LeaderboardConfiguration } from './enums/LeaderboardConfiguration';

const basePath = process.env.ARSNOVA_CLICK_BACKEND_BASE_PATH  || '';
const portInternal = +parseInt(process.env.ARSNOVA_CLICK_BACKEND_PORT_INTERNAL, 10) || 3010;
const scuttlebuttPort = +parseInt(process.env.ARSNOVA_CLICK_BACKEND_PORT_SCUTTLEBUTT, 10) || portInternal + 10 || 3020;
const prometheusPort = +process.env.ARSNOVA_CLICK_BACKEND_PORT_PROMETHEUS || scuttlebuttPort + 10 || 3030;
const portExternal = +process.env.ARSNOVA_CLICK_BACKEND_PORT_EXTERNAL || portInternal;
const routePrefix = process.env.ARSNOVA_CLICK_BACKEND_ROUTE_PREFIX || '';
const rewriteAssetCacheUrl = process.env.ARSNOVA_CLICK_BACKEND_REWRITE_ASSET_CACHE_URL || `http://${hostname()}:${portExternal}${routePrefix}`;
const leaderboardAlgorithm = process.env.LEADERBOARD_ALGORITHM || LeaderboardConfiguration.PointBased;

const amqpProtocol = process.env.AMQP_PROTOCOL || 'amqp';
const amqpHostname = process.env.AMQP_HOSTNAME || 'localhost';
const amqpVhost = process.env.AMQP_VHOST || '/';
const amqpUser = process.env.AMQP_USER || 'guest';
const amqpPassword = process.env.AMQP_PASSWORD || 'guest';
const amqpManagementApiProtocol = process.env.AMQP_MANAGEMENT_API_PROTOCOL || 'http:';
const amqpManagementApiHost = process.env.AMQP_MANAGEMENT_API_HOST || amqpHostname;
const amqpManagementApiPort = process.env.AMQP_MANAGEMENT_API_PORT || '15672';
const amqpManagementUser = process.env.AMQP_MANAGEMENT_USER || amqpUser;
const amqpManagementPassword = process.env.AMQP_MANAGEMENT_PASSWORD || amqpPassword;

const frontendGitlabId = parseInt(process.env.GITLAB_FRONTEND_PROJECT_ID, 10);
const backendGitlabId = parseInt(process.env.GITLAB_BACKEND_PROJECT_ID, 10);
const gitlabLoginToken = process.env.GITLAB_TOKEN;
const gitlabHost = process.env.GITLAB_HOST;
const gitlabTargetBranch = process.env.GITLAB_TARGET_BRANCH ?? 'master';


export const publicSettings = {
  leaderboardAlgorithm,
  cacheQuizAssets: true,
  createQuizPasswordRequired: false,
  limitActiveQuizzes: Infinity,
};

const homedir = path.join(require('os').homedir(), '.arsnova-click-v2');

export const settings = {
  appName: 'arsnova-click-v2-backend',
  appVersion: '__VERSION__',
  homedir,
  specFile: path.join(homedir, 'spec.json'),
  port: {
    app: portInternal,
    scuttlebutt: scuttlebuttPort,
    prometheus: prometheusPort,
  },
  routePrefix: routePrefix,
  rewriteAssetCacheUrl: rewriteAssetCacheUrl,
  pathToAssets: path.join(__dirname, basePath, process.env.NODE_ENV === 'production' ? '' : '..', 'assets'),
  pathToMigrations: path.join(__dirname, basePath, process.env.NODE_ENV === 'production' ? '' : '..', 'db-migration'),
  jwtSecret: 'arsnova.click-v2',
  createQuizPassword: 'abc',
  amqp: {
    protocol: amqpProtocol,
    hostname: amqpHostname,
    vhost: amqpVhost,
    user: amqpUser,
    password: amqpPassword,
    managementApi: {
      host: amqpManagementApiHost,
      protocol: amqpManagementApiProtocol,
      port: amqpManagementApiPort,
      user: amqpManagementUser,
      password: amqpManagementPassword,
    },
  },
  gitlab: {
    frontend: frontendGitlabId,
    backend: backendGitlabId,
    loginToken: gitlabLoginToken,
    host: gitlabHost,
    targetBranch: gitlabTargetBranch,
  },
};

