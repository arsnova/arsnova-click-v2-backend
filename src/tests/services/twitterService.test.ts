import { suite, test } from '@testdeck/mocha';
import sinon from 'sinon';
import DbDAO from '../../db/DbDAO';
import MongoDBConnector from '../../db/MongoDBConnector';
import TwitterService from '../../services/TwitterService';
import SettingsService from '../../services/SettingsService';
import { ISettings } from '../../interfaces/settings/ISettings';
import SettingsDAO from '../../db/SettingsDAO';

@suite




class TwitterServiceTestSuite {
  private _runCallback;

  public async before(): Promise<void> {
    const sandbox = sinon.createSandbox();
    sandbox.stub(MongoDBConnector, 'rabbitEventEmitter').value({
      on: (evt, callback) => this._runCallback = callback,
    });
    SettingsService.twitter = {
      _id: 'tt',
      configName: 'twitter',
      config: {
        twitterEnabled: true,
        twitterConsumerKey: '',
        twitterConsumerSecret: '',
        twitterBearerToken: '',
        twitterAccessTokenKey: '',
        twitterAccessTokenSecret: '',
        twitterSearchKey: 'arsnova.click OR arsnovaclick OR arsnova-click OR @arsnovaclick OR #arsnovaclick OR #arsnova-click'
      }
    };
  }

  public async after(): Promise<void> {
    await Promise.all(Object.keys(DbDAO.dbCon.collections).map(c => DbDAO.dbCon.collection(c).deleteMany({})));
  }

  @test
  public async testPrivateFunctions(): Promise<void> {
    TwitterService.run();
  }
}
