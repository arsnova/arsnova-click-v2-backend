import {CronJob} from 'cron';
import QuizDAO from '../db/QuizDAO';
import UserDAO from '../db/UserDAO';
import LoggerService from '../services/LoggerService';

class DbCleanupScheduler {
  private static _instance: DbCleanupScheduler;
  private readonly _scheduler = '* * * 2 0 0';
  private readonly _job: CronJob;
  private readonly _users: Array<string> = ['teacher1'];

  constructor() {
    this._job = new CronJob(this._scheduler, async () => {

      const privateKeys: Array<string> = await Promise.all(this._users.map(async user => (await UserDAO.getUser(user)).privateKey));
      const result = await QuizDAO.clearQuizzesByPrivateKey(privateKeys);
      LoggerService.info(`DbCleanupScheduler completed work. Modified ${result.nModified} documents`);

    }, null, false, 'Europe/Berlin');
  }

  public start(): void {
    this._job.start();
  }

  public static getInstance(): DbCleanupScheduler {
    if (!this._instance) {
      this._instance = new DbCleanupScheduler();
    }

    return this._instance;
  }
}

export default DbCleanupScheduler.getInstance();
