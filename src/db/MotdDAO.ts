import { Document, DocumentDefinition } from 'mongoose';
import { IMotd } from '../interfaces/motd/IMotd';
import {
  MessageOfTheDayItem,
  MessageOfTheDayModel,
} from '../models/motd/MessageOfTheDayItem';
import { AbstractDAO } from './AbstractDAO';

class MotdDAO extends AbstractDAO {
  constructor() {
    super();
  }
  public static getInstance(): MotdDAO {
    if (!this.instance) {
      this.instance = new MotdDAO();
    }

    return this.instance;
  }

  public async getStatistics(): Promise<{ [key: string]: number }> {
    return {};
  }

  /**
   * Find all messages of the day
   */
  public async getAllMessagesOfTheDay(): Promise<Array<IMotd>> {
    return MessageOfTheDayModel.find().lean().exec();
  }

  /**
   * Create new message ot the day
   * @param doc New message to create
   */
  public async createNewMessageOfTheDay(doc: MessageOfTheDayItem): Promise<Document & MessageOfTheDayItem> {
    delete doc['_id'];
    delete doc['id'];

    const lastMotd = await MessageOfTheDayModel.findOne({}, {mid: 1}).sort({mid: -1}).exec();
    doc.mid = lastMotd.mid + 1;

    return MessageOfTheDayModel.create(doc);
  }

  /**
   * Update specific message of the day based on the id
   * @param updatedMessageOfDay Updated information
   */
  public async updateSpecificMessageOfTheDay(
    updatedMessageOfDay: IMotd
  ): Promise<MessageOfTheDayItem> {
    try {
      const filter = {
        _id: updatedMessageOfDay._id,
        mid: updatedMessageOfDay.mid,
      };
      const updateOps = {
        $set: {
          title: updatedMessageOfDay.title,
          content: updatedMessageOfDay.content,
          expireAt: updatedMessageOfDay.expireAt,
          isPinned: updatedMessageOfDay.isPinned,
        },
      };
      const dbStatusInformationen = await MessageOfTheDayModel.updateOne(
        filter,
        updateOps
      ).exec();

      if (
        dbStatusInformationen.n &&
        dbStatusInformationen.nModified === 1 &&
        dbStatusInformationen.ok === 1
      ) {
        const result = await MessageOfTheDayModel.findOne(filter);
        return result['_doc'];
      } else {
        return null;
      }
    } catch (error) {
      console.error(
        'updateSpecificMessageOfTheDay() -> Could not update message of the day: ',
        error
      );
      return null;
    }
  }

  /**
   * Delete one specific message of the day based on the _id
   * @param motdId Id of the item from message of the day
   */
  public async deleteSpecificMessageOfTheDay(motdId: String): Promise<boolean> {
    const filter = { _id: motdId };
    const result = await MessageOfTheDayModel.deleteOne(filter);
    console.log('deleteSpecificMessageOfTheDay()', result);
    return true;
  }
}

export default MotdDAO.getInstance();
