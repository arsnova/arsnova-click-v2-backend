import { ObjectId } from 'bson';
import { Document } from 'mongoose';
import { IPCExchange } from '../enums/IPCExchange';
import { SettingsItem, SettingsModel } from '../models/settings/SettingsItem';
import SettingsService from '../services/SettingsService';
import { AbstractDAO } from './AbstractDAO';

class SettingsDAO extends AbstractDAO {
  constructor() {
    super();
  }
  public static getInstance(): SettingsDAO {
    if (!this.instance) {
      this.instance = new SettingsDAO();
    }

    return this.instance;
  }

  public async getStatistics(): Promise<{ [key: string]: number }> {
    return {};
  }

  /**
   * Create new settings
   * @param newSettingsToCreate New settings to create
   */
  public async createNewSettings(
    newSettingsToCreate: SettingsItem
  ): Promise<Document & SettingsItem> {
    try {
      const filter = {
        configName: {
          $eq: newSettingsToCreate.configName,
        },
      };
      const check = await SettingsModel.findOne(filter).exec();

      if (check) {
        return null;
      }

      newSettingsToCreate._id = new ObjectId().toString();
      const result = await SettingsModel.create(newSettingsToCreate as any);
      return result['_doc'];
    } catch (error) {
      console.error(
        'createNewSettings() -> Could not create new settings',
        error
      );
      return null;
    }
  }

  /**
   * Update specific settings based on the id
   * @param updateSettings Updated information
   */
  public async updateSettingsInformation(
    updateSettings: SettingsItem
  ): Promise<SettingsItem> {
    try {
      const filter = {
        configName: updateSettings.configName,
      };
      const updateOps = {
        $set: {
          config: updateSettings.config,
        },
      };
      const dbStatusInformationen = await SettingsModel.updateOne(
        filter,
        updateOps
      ).exec();

      if (
        dbStatusInformationen.n &&
        dbStatusInformationen.nModified === 1 &&
        dbStatusInformationen.ok === 1
      ) {
        const result = (await this.getConfigurationByConfigName(updateSettings.configName)).toJSON();

        process.send({ message: IPCExchange.ReloadSettings, data: null });
        return result;

      } else {
        console.error('updateSettingsInformation() -> Could not update settings: ', dbStatusInformationen);
        return null;
      }
    } catch (error) {
      console.error(
        'updateSettingsInformation() -> Could not update settings: ',
        error
      );
    }
  }

  /**
   * Delete settings based on the _id
   * @param settingId Id of configuration
   */
  public async deleteSettings(settingId: String): Promise<boolean> {
    const filter = { _id: settingId };
    const result = await SettingsModel.deleteOne(filter);
    console.log('deleteSettings()', result);
    return true;
  }

  /**
   * @param configName Name of configuration
   */
  public async getConfigurationByConfigName(configName: string): Promise<Document & SettingsItem> {
    return SettingsModel.findOne({configName}).exec();
  }

}

export default SettingsDAO.getInstance();
