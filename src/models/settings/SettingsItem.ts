import {
    getModelForClass,
    index,
    modelOptions,
    prop,
    Severity,
} from '@typegoose/typegoose';
import {IsString, IsObject} from 'class-validator';
import DbDAO from '../../db/DbDAO';
import {DbCollection} from '../../enums/DbOperation';
import {ISettings} from '../../interfaces/settings/ISettings';

@index(
    {
        _id: 1,
        configName: 1
    },
    {
        unique: true,
        collation: {
            locale: 'en',
            strength: 1,
        },
    }
)//
@modelOptions({options: {allowMixed: Severity.ALLOW}}) //
export class SettingsItem implements ISettings {
    @prop() public _id?: string;
    @prop() @IsString() public configName: string;
    @prop() @IsObject() public config: any;
}

export const SettingsModel = getModelForClass(SettingsItem, {
    schemaOptions: {
        collection: DbCollection.Settings,
        timestamps: true,
    },
    existingConnection: DbDAO.dbCon,
    options: {
        runSyncIndexes: true,
        allowMixed: Severity.ALLOW,
    },
});

