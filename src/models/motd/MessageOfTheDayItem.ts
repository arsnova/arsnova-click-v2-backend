import {
  getModelForClass,
  index,
  modelOptions,
  prop,
  Severity,
} from '@typegoose/typegoose';
import { IsString, IsDate, IsNumber, IsBoolean } from 'class-validator';
import DbDAO from '../../db/DbDAO';
import { DbCollection } from '../../enums/DbOperation';
import { IMotd } from '../../interfaces/motd/IMotd';

@index(
  {
    _id: 1,
    mid: 1,
    title: 1,
  },
  {
    unique: true,
    collation: {
      locale: 'en',
      strength: 1,
    },
  }
)
@index(
  {
    expireAt: 1,
  },
  {
    expireAfterSeconds: 0,
  }
) //
@modelOptions({ options: { allowMixed: Severity.ALLOW } }) //
export class MessageOfTheDayItem implements IMotd {
  @prop() @IsNumber() public mid?: number;
  @prop() @IsString() public title: string;
  @prop() @IsString() public content: string;
  @prop() @IsDate() public createdAt: Date;
  @prop() @IsDate() public updatedAt: Date;
  @prop() @IsDate() public expireAt: Date;
  @prop() @IsBoolean() public isPinned: Boolean;
}

export const MessageOfTheDayModel = getModelForClass(MessageOfTheDayItem, {
  schemaOptions: {
    collection: DbCollection.MessageOfTheDay,
    timestamps: true,
  },
  existingConnection: DbDAO.dbCon,
  options: {
    runSyncIndexes: true,
    allowMixed: Severity.ALLOW,
  },
});
