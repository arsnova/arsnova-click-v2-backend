import { EventEmitter } from 'events';
import fs from 'fs';
import SettingsDAO from '../db/SettingsDAO';
import { IEnvironment } from '../interfaces/settings/IEnvironment';
import { IGit } from '../interfaces/settings/IGit';
import { ITwitter } from '../interfaces/settings/ITwitter';
import { IMiscellaneous } from '../interfaces/settings/IMiscellaneous';
import { IWebNotifications } from '../interfaces/settings/IWebNotifications';
import { ISettings } from '../interfaces/settings/ISettings';
import { ApiRouter } from '../routers/rest/ApiRouter';
import { settings } from '../settings';

class SettingsService {

  public static getInstance(): SettingsService {
    if (!this._instance) {
      this._instance = new SettingsService();
    }

    return this._instance;
  }
  private static _instance: SettingsService;

  public environment: IEnvironment = null;
  public git: IGit = null;
  public twitter: ITwitter = null;
  public miscellaneous: IMiscellaneous = null;
  public webNotifications: IWebNotifications = null;
  public readonly settingsChanged = new EventEmitter();

  constructor() {
  }

  public async loadSettings(): Promise<void> {
    this.environment = await SettingsDAO.getConfigurationByConfigName('front_env');
    if (!this.environment.config.limitActiveQuizzes) {
      this.environment.config.limitActiveQuizzes = Infinity;
    }
    settings.appVersion = this.environment.config.backendVersion;
    fs.unlink(settings.specFile, () => {});

    this.git = await SettingsDAO.getConfigurationByConfigName('git');
    this.twitter = await SettingsDAO.getConfigurationByConfigName('twitter');
    this.miscellaneous = await SettingsDAO.getConfigurationByConfigName('misc');
    this.webNotifications = await SettingsDAO.getConfigurationByConfigName('web_notification');
  }
}

export default SettingsService.getInstance();
