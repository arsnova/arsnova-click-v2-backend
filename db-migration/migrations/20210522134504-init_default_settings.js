module.exports = {
  async up(db, client) {
    const availableQuizThemes = ['Material', 'blackbeauty'];

    await db.collection('settings').insertMany([
      {
        configName: 'front_env',
        config: {
          title: 'arsnova.click',
          appName: 'arsnova.click',
          sentryDSN: '',
          backendVersion: '2.0.1',
          leaderboardAmount: 5,
          enableBonusToken: true,
          readingConfirmationEnabled: true,
          confidenceSliderEnabled: true,
          infoAboutTabEnabled: true,
          infoProjectTabEnabled: true,
          infoBackendApiEnabled: true,
          requireLoginToCreateQuiz: true,
          forceQuizTheme: true,
          loginMechanism: ['UsernamePassword'],
          showLoginButton: true,
          showJoinableQuizzes: true,
          showPublicQuizzes: true,
          persistQuizzes: true,
          availableQuizThemes,
          defaultTheme: availableQuizThemes[0],
          darkModeCheckEnabled: true,
          enableQuizPool: true,
          showInfoButtonsInFooter: true,
          vapidPublicKey: '',
          markdownFilePostfix: 'thm',
          loginButtonLabelConfiguration: 'administration',
          leaderboardAlgorithm: 'PointBased',
          cacheQuizAssets: true,
          createQuizPasswordRequired: false,
          limitActiveQuizzes: 0,
          twitterEnabled: false,
          defaultQuizSettings: {
            answers: {
              answerText: '',
              isCorrect: false, // Must be false so that surveys are valid (check if isCorrect is false)
              configCaseSensitive: false,
              configTrimWhitespaces: false,
              configUseKeywords: false,
              configUsePunctuation: false,
            },
            question: {
              dispayAnswerText: true,
              showOneAnswerPerRow: true,
              questionText: '',
              timer: 30,
              multipleSelectionEnabled: true,
              rangeMin: 0,
              rangeMax: 60,
              correctValue: 30,
              answerOptionList: [],
              tags: [],
              requiredForToken: true,
              difficulty: 5
            },
            sessionConfig: {
              music: {
                enabled: {
                  lobby: true,
                  countdownRunning: true,
                  countdownEnd: true,
                },
                shared: {
                  lobby: false,
                  countdownRunning: false,
                  countdownEnd: false,
                },
                volumeConfig: {
                  global: 60,
                  lobby: 60,
                  countdownRunning: 60,
                  countdownEnd: 60,
                  useGlobalVolume: true,
                },
                titleConfig: {
                  lobby: 'Song3',
                  countdownRunning: 'Song1',
                  countdownEnd: 'Song1',
                },
              },
              nicks: {
                memberGroups: [],
                maxMembersPerGroup: 10,
                autoJoinToGroup: false,
                blockIllegalNicks: true,
                selectedNicks: [],
              },
              theme: availableQuizThemes[0],
              readingConfirmationEnabled: true,
              showResponseProgress: true,
              confidenceSliderEnabled: true,
              cacheQuizAssets: false,
            },
          },
        }
      },
      {
        configName: 'twitter',
        config: {
          twitterEnabled: false,
          twitterConsumerKey: '',
          twitterConsumerSecret: '',
          twitterBearerToken: '',
          twitterAccessTokenKey: '',
          twitterAccessTokenSecret: '',
          twitterSearchKey: 'arsnova.click OR arsnovaclick OR arsnova-click OR @arsnovaclick OR #arsnovaclick OR #arsnova-click',
        }
      },
      {
        configName: 'misc',
        config: {
          chromiumPath: '/usr/bin/chromium-browser',
          projectEmail: '',
          logoFilename: 'logo_transparent.png',
        }
      },
      {
        configName: 'git',
        config: {
          frontendGitlabId: '',
          backendGitlabId: '',
          gitlabLoginToken: '',
          gitlabHost: '',
          gitlabTargetBranch: 'master',
        }
      },
      {
        configName: 'web_notification',
        config: {
          vapidPublicKey: '',
          vapidPrivateKey: '',
        }
      },
    ]);
    await db.collection('settings').createIndex('configName', {unique: true});
  },

  async down(db, client) {
    await db.collection('settings').deleteMany();
    await db.collection('settings').dropCollection();
  }
};
